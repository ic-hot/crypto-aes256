package ic.util.crypto.aes256.impl


import javax.crypto.Cipher


internal fun createCipher() : Cipher {

	return Cipher.getInstance("AES/CBC/NoPadding")

}