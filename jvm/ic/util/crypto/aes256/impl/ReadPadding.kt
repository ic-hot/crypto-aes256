package ic.util.crypto.aes256.impl


import java.io.IOException

import ic.base.throwables.IoException
import ic.base.throwables.AccessException

import ic.util.crypto.aes256.Aes256DecryptByteInput


@Throws(IoException::class, AccessException::class)
internal fun Aes256DecryptByteInput.readPadding() {

	try {

		repeat(chunkLengthInBytes) {

			val i = cipherInputStream.read()
			if (i != 0) {
				cipherInputStream.close()
				isClosed = true
				throw AccessException
			}

		}

	} catch (e: IOException) {

		throw IoException

	}

}