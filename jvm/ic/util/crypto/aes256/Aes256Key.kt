package ic.util.crypto.aes256


import ic.base.arrays.ext.copy
import ic.base.arrays.ext.length
import ic.base.assert.assert
import ic.base.strings.ext.toByteArray
import ic.util.text.charset.Charset.Companion.Utf8


class Aes256Key (

	bytes : ByteArray

) {

	init {

		assert { bytes.length == lengthInBytes }

	}

	val asByteArray : ByteArray = bytes

	companion object {

		const val lengthInBytes = 256 / Byte.SIZE_BITS

		fun fromPassword (password: String) : Aes256Key {
			return Aes256Key(
				bytes = password.toByteArray(charset = Utf8).copy(length = lengthInBytes)
			)
		}

	}

}