package ic.util.crypto.aes256


import java.io.InputStream
import java.io.IOException
import javax.crypto.Cipher
import javax.crypto.CipherInputStream

import ic.base.primitives.int32.Int32
import ic.base.primitives.int32.ext.asByte
import ic.base.throwables.End
import ic.base.throwables.End.End
import ic.base.throwables.IoException
import ic.stream.input.ByteInput
import ic.stream.input.ext.asInputStream
import ic.base.throwables.AccessException

import ic.util.crypto.aes256.impl.*


class Aes256DecryptByteInput

	@Throws(IoException::class, AccessException::class)
	constructor (
		sourceInputStream : InputStream,
		key : Aes256Key
	)

	: ByteInput

{


	internal val cipherInputStream = CipherInputStream(
		sourceInputStream,
		createCipher().apply {
			init(
				Cipher.DECRYPT_MODE,
				createSecretKeySpec(key),
				createInitVectorSpec()
			)
		}
	)

	internal var isClosed : Boolean = false


	init {
		readPadding()
	}


	internal var bytesLeftInChunk : Int32 = 0

	internal var isLastChunk : Boolean = false


	@Throws(IoException::class, End::class)
	override fun getNextByteOrThrowEnd() : Byte {
		if (isClosed) End()
		if (bytesLeftInChunk == 0) {
			if (isLastChunk) {
				cipherInputStream.close()
				isClosed = true
				End()
			} else {
				readBytesLeft()
				if (bytesLeftInChunk < 255) {
					if (bytesLeftInChunk == 0) {
						cipherInputStream.close()
						isClosed = true
						End()
					} else {
						isLastChunk = true
					}
				}
			}
		}
		try {
			val i = cipherInputStream.read()
			if (i == -1) {
				cipherInputStream.close()
				isClosed = true
				End()
			}
			val byte = i.asByte
			bytesLeftInChunk--
			return byte
		} catch (_: IOException) {
			throw IoException
		}
	}


	@Throws(IoException::class)
	override fun close() {
		try {
			cipherInputStream.close()
			isClosed = true
		} catch (_: IOException) {
			throw IoException
		}
	}


	@Throws(IoException::class, AccessException::class)
	constructor(
		sourceInput : ByteInput,
		key : Aes256Key
	) : this(
		sourceInputStream = sourceInput.asInputStream,
		key = key
	)


}