package ic.util.crypto.aes256


import java.io.IOException
import java.io.OutputStream
import javax.crypto.Cipher.ENCRYPT_MODE
import javax.crypto.CipherOutputStream

import ic.base.arrays.bytes.ByteArray
import ic.base.primitives.int32.Int32
import ic.base.throwables.IoException
import ic.stream.output.ByteOutput
import ic.stream.output.ext.asOutputStream

import ic.util.crypto.aes256.impl.*


class Aes256EncryptByteOutput

	@Throws(IoException::class)
	constructor (
		sourceOutputStream : OutputStream,
		key : Aes256Key
	)

	: ByteOutput

{


	internal val cipherOutputStream = CipherOutputStream(
		sourceOutputStream,
		createCipher().apply {
			init(
				ENCRYPT_MODE,
				createSecretKeySpec(key),
				createInitVectorSpec()
			)
		}
	)


	init {
		writePadding()
	}



	internal var chunkBytesCount : Int32 = 0

	internal val chunk : ByteArray = ByteArray(length = chunkLengthInBytes)


	@Throws(IoException::class)
	override fun putByte (byte: Byte) {
		val relativeIndexInChunk = chunkBytesCount++
		val absoluteIndexInChunk = 1 + relativeIndexInChunk
		chunk[absoluteIndexInChunk] = byte
		if (chunkBytesCount == 255) {
			writeAndClearChunk()
		}
	}


	@Throws(IoException::class)
	override fun close() {
		if (chunkBytesCount > 0) {
			writeAndClearChunk()
		}
		try {
			cipherOutputStream.flush()
			cipherOutputStream.close()
		} catch (e: IOException) {
			throw IoException
		}
	}


	@Throws(IoException::class)
	override fun cancel() {
		try {
			cipherOutputStream.close()
		} catch (_: IOException) {
			throw IoException
		}
	}


	@Throws(IoException::class)
	constructor (
		sourceOutput : ByteOutput,
		key : Aes256Key
	) : this (
		sourceOutputStream = sourceOutput.asOutputStream,
		key = key
	)


}