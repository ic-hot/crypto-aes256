package ic.stream.input.ext


import ic.stream.input.ByteInput

import ic.util.crypto.aes256.Aes256DecryptByteInput
import ic.util.crypto.aes256.Aes256Key


@Suppress("NOTHING_TO_INLINE")
inline fun ByteInput.decryptAes256 (key: Aes256Key) : ByteInput {

	return Aes256DecryptByteInput(
		sourceInput = this,
		key = key
	)

}


@Suppress("NOTHING_TO_INLINE")
inline fun ByteInput.decryptAes256 (password: String) : ByteInput {

	return Aes256DecryptByteInput(
		sourceInput = this,
		key = Aes256Key.fromPassword(password)
	)

}