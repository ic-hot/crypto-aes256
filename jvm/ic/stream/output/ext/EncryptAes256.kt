package ic.stream.output.ext


import ic.stream.output.ByteOutput

import ic.util.crypto.aes256.Aes256EncryptByteOutput
import ic.util.crypto.aes256.Aes256Key


@Suppress("NOTHING_TO_INLINE")
inline fun ByteOutput.encryptAes256 (key: Aes256Key) : ByteOutput {

	return Aes256EncryptByteOutput(
		sourceOutput = this,
		key = key
	)

}


@Suppress("NOTHING_TO_INLINE")
inline fun ByteOutput.encryptAes256 (password: String) : ByteOutput {

	return Aes256EncryptByteOutput(
		sourceOutput = this,
		key = Aes256Key.fromPassword(password)
	)

}